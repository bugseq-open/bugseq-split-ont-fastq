# bugseq-split-ont-fastq

## Intro

Takes nanopore demultiplexed FASTQ files and splits them by channel. By default splits channels 1-256 and 257-512. Input files must be gzipped.

## Usage

```python3
bugseq-split-ont-fastq.py --input_dir my_input_dir --output_dir my_output_dir
```