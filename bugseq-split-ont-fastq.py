#!/usr/bin/env python3

import argparse
import gzip
import logging
import re
from pathlib import Path


# copy StreamModeError and FastqGeneralIterator from biopython: https://github.com/biopython/biopython/blob/2ee477e49623cab39b29721428321a2a6669164e/Bio/SeqIO/QualityIO.py#L823-L991
class StreamModeError(ValueError):
    """Incorrect stream mode (text vs binary).
    This error should be raised when a stream (file or file-like object)
    argument is in text mode while the receiving function expects binary mode,
    or vice versa.
    """


def FastqGeneralIterator(source):
    try:
        handle = open(source)
    except TypeError:
        handle = source
        if handle.read(0) != "":
            raise StreamModeError("Fastq files must be opened in text mode") from None
    try:
        try:
            line = next(handle)
        except StopIteration:
            return  # Premature end of file, or just empty?

        while True:
            if line[0] != "@":
                raise ValueError(
                    "Records in Fastq files should start with '@' character"
                )
            title_line = line[1:].rstrip()
            seq_string = ""
            # There will now be one or more sequence lines; keep going until we
            # find the "+" marking the quality line:
            for line in handle:
                if line[0] == "+":
                    break
                seq_string += line.rstrip()
            else:
                if seq_string:
                    raise ValueError("End of file without quality information.")
                else:
                    raise ValueError("Unexpected end of file")
            # The title here is optional, but if present must match!
            second_title = line[1:].rstrip()
            if second_title and second_title != title_line:
                raise ValueError("Sequence and quality captions differ.")
            # This is going to slow things down a little, but assuming
            # this isn't allowed we should try and catch it here:
            if " " in seq_string or "\t" in seq_string:
                raise ValueError("Whitespace is not allowed in the sequence.")
            seq_len = len(seq_string)

            # There will now be at least one line of quality data, followed by
            # another sequence, or EOF
            line = None
            quality_string = ""
            for line in handle:
                if line[0] == "@":
                    # This COULD be the start of a new sequence. However, it MAY just
                    # be a line of quality data which starts with a "@" character.  We
                    # should be able to check this by looking at the sequence length
                    # and the amount of quality data found so far.
                    if len(quality_string) >= seq_len:
                        # We expect it to be equal if this is the start of a new record.
                        # If the quality data is longer, we'll raise an error below.
                        break
                    # Continue - its just some (more) quality data.
                quality_string += line.rstrip()
            else:
                if line is None:
                    raise ValueError("Unexpected end of file")
                line = None

            if seq_len != len(quality_string):
                raise ValueError(
                    "Lengths of sequence and quality values differs for %s (%i and %i)."
                    % (title_line, seq_len, len(quality_string))
                )

            # Return the record and then continue...
            yield (title_line, seq_string, quality_string)

            if line is None:
                break
    finally:
        if handle is not source:
            handle.close()


# End of copying from biopython

channel_regex = re.compile(" ch=([\d]+) ")
barcode_regex = re.compile("(barcode[0-9a-z]+|unclassified)")


def get_channel(title):
    channel = int(channel_regex.search(title)[1])
    return channel


def split_fastq(fastq, input_dir, output_dir):
    rel_path = fastq.relative_to(input_dir)
    nested_output_dir = output_dir.joinpath(rel_path.parent)
    nested_output_dir.mkdir(parents=True, exist_ok=True)
    output_fastq_1_256 = nested_output_dir.joinpath(
        barcode_regex.sub(r"\1x1to256", rel_path.name)
    )
    output_fastq_257_512 = nested_output_dir.joinpath(
        barcode_regex.sub(r"\1x257to512", rel_path.name)
    )
    with gzip.open(fastq, "rt") as reader, gzip.open(
        output_fastq_1_256, "wt", compresslevel=1
    ) as output_1, gzip.open(output_fastq_257_512, "wt", compresslevel=1) as output_2:
        fastq_parser = FastqGeneralIterator(reader)
        for title, seq, quality in fastq_parser:
            channel = get_channel(title)
            if 1 <= channel <= 256:
                output_1.write(f"@{title}\n{seq}\n+\n{quality}\n")
            elif 257 <= channel <= 512:
                output_2.write(f"@{title}\n{seq}\n+\n{quality}\n")
            else:
                raise ValueError(f"Channel {channel} is out of expected range of 1-512")


def main(input_dir, output_dir):
    for fastq in input_dir.rglob("*.fastq.gz"):
        logging.info(f"Splitting {str(fastq)}")
        split_fastq(fastq, input_dir, output_dir)


parser = argparse.ArgumentParser(description="Split FASTQ by channel")
parser.add_argument(
    "-i", "--input_dir", type=Path, help="Input directory. All FASTQs must be gzipped."
)
parser.add_argument("-o", "--output_dir", type=Path, help="Output directory")

args = parser.parse_args()
main(args.input_dir, args.output_dir)
